#!/bin/sh

export PATH=/sbin:/usr/sbin:$PATH
DEBOS_CMD=debos
IMG_FILE="debian-pinephone-`date +%Y%m%d`.img"
ARGS=
image_only=
do_compress=
username=
password=
memory=
use_docker=

while getopts "izupdm:h:f:" opt
do
  case "$opt" in
    i ) image_only=1 ;;
    z ) do_compress=1 ;;
    u ) username="$OPTARG" ;;
    p ) password="$OPTARG" ;;
    d ) use_docker=1 ;;
    m ) memory="$OPTARG" ;;
    h ) http_proxy="$OPTARG" ;;
    f ) ftp_proxy="$OPTARG" ;;
  esac
done

if [ "$use_docker" ]; then
  DEBOS_CMD=docker
  ARGS="run --rm --interactive --tty --device /dev/kvm --workdir /recipes \
  --mount "type=bind,source=$(pwd),destination=/recipes" \
  --security-opt label=disable godebos/debos"
fi
if [ "$username" ]; then
  ARGS="$ARGS -t username:\"$username\""
fi

if [ "$password" ]; then
  ARGS="$ARGS -t password:\"$password\""
fi

if [ "$http_proxy" ]; then
  ARGS="$ARGS -e http_proxy:\"$http_proxy\""
fi

if [ "$ftp_proxy" ]; then
  ARGS="$ARGS -e ftp_proxy:\"$ftp_proxy\""
fi

if [ "$memory" ]; then
  ARGS="$ARGS --memory \"$memory\""
fi

ARGS="$ARGS --scratchsize=8G"

if [ ! "$image_only" ]; then
  $DEBOS_CMD $ARGS bootstrap.yaml || exit 1
fi
$DEBOS_CMD $ARGS -t image:$IMG_FILE image.yaml

if [ "$do_compress" ]; then
  echo "Compressing $IMG_FILE..."
  gzip --keep --force $IMG_FILE
fi
