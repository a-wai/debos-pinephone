#!/bin/sh

# Select only en_US UTF-8 locale (at least for now)
sed -i -e '/en_US\.UTF-8/s/^# //g' /etc/locale.gen

# Generate locales
locale-gen
